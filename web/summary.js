// fills the summary table with transactions' info between selected dates
function getAllTransactions(startDate, endDate) {
    $('.summary-table').css('visibility', 'hidden');
    if (endDate < startDate) {
        $('#alert-text-dates').css('visibility', 'visible');
    } else {
        $('#alert-text-dates').css('visbility', 'hidden');
        var urlName = "http://localhost:8080/rest/transactions/byUser/" + startDate + "/" + endDate;
        $.getJSON(urlName, function(transactions){
            allTransactions = transactions;
            if (transactions.length === 0 || transactions === undefined) {
                $('#alert-text-dates').text("Antud vahemikus pole tehinguid sisestatud.");
                $('#alert-text-dates').css('visibility', 'visible');
            } else {
                var tableBody = document.getElementById("transactionsTableBody");
                tableBody.innerHTML = displayTransactionsTable(transactions);
            }
        });
    }
}

// table to summary.html
function displayTransactionsTable(transactions) {
    $('.summary-table').css('visibility', 'visible');
    var tableContent = "";
    for (var i = 0; i < transactions.length; i++) {
        var dateOfTransactionStringId = 'dateOfTransactionString' + i;
        var descriptionId = 'description' + i;
        var amountId = 'amount' + i;
        var categoryNameId = 'categoryId' + i;
        var buttonCell = 'buttonCell' + i;
        var modifyTransactionButton = 'modifyTransactionButton' + i;
        var deleteTransactionButton = 'deleteTransactionButton' + i;
        tableContent += "<tr><td>" + (i + 1) +
            "</td><td id='" + dateOfTransactionStringId + "'>" + transactions[i].dateOfTransactionString +
            "</td><td id='" + descriptionId + "'>" + transactions[i].description +
            "</td><td id='" + amountId + "' class='amountCell'>" + transactions[i].amount.toFixed(2) +
            "</td><td id='" + categoryNameId + "'>" + transactions[i].category.categoryName +
            "</td><td id='" + buttonCell + "'>" +
            "<button type=\"button\" onClick='modifyTransactionField(" + transactions[i].transactionId + ", \""
                + transactions[i].dateOfTransactionString + "\", \"" + transactions[i].description + "\", " +
                transactions[i].amount.toFixed(2) + ", \"" + transactions[i].category.categoryName + "\", " + transactions[i].category.categoryId + ", " +
                transactions[i].user.userId + ", " + transactions[i].group.groupId + ", " + i + ")' " +
                "class=\"btn btn-outline-secondary btn-modify\" id = \"" + modifyTransactionButton + "\">Muuda</button>" +
            "<button type=\"button\" onClick='askIfDelete(" + transactions[i].transactionId + ")' class=\"btn btn-outline-danger btn-delete\" " +
                "id = \"" + deleteTransactionButton + "\">X</button>" + "</td></tr>";
        $('#buttonCell' + i).css('background-color', 'black');
    }
    return tableContent;
}

//displays the list of categories with one selected
function getAllCategoriesOneSelected(selectedId, dropDownId) {
    $.getJSON("http://localhost:8080/rest/categories/groupId", function(categories){
        var selectionContent = document.getElementById(dropDownId);
        var filledContent = "";
        for(var i = 0; i < categories.length; i++) {
            if (categories[i].categoryId === selectedId) {
                filledContent += "<option value=\"" + categories[i].categoryId + "\" selected=\"selected\">" + categories[i].categoryName + "</option>";
            } else {
                filledContent += "<option value=\"" + categories[i].categoryId + "\">" + categories[i].categoryName + "</option>";
            }
        }
        selectionContent.innerHTML = filledContent;
    });
}

//removes a transaction from database
function deleteTransaction(transactionId) {
    var deleteTransactionJson = {"transactionId": transactionId};

    $.ajax({
        url: "http://localhost:8080/rest/transactions",
        type: "DELETE",
        data: JSON.stringify(deleteTransactionJson),
        contentType: "application/json; charset=utf-8",
        success: function () {
            confirmDates();
            $('.delete-transaction-window').css('display', 'none');
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    })
}

//modifies the details of a transaction
function modifyTransactionField(transactionId, date, description, amount, category, categoryId2, userId, groupId, rowNr) {
    var amountId = document.getElementById('amount' + rowNr);
    amountId.innerHTML = "<div class=\"form-group\"><input type=\"text\" value=\"" + amount + "\" class=\"form-control\" " +
        "id=\"sumOfTransaction\"></div>";

    var dateOfTransactionStringId = document.getElementById('dateOfTransactionString' + rowNr);
    dateOfTransactionStringId.innerHTML = "<div class=\"form-group\"><input type=\"text\" value=\"" + date + "\" class=\"form-control datepicker\" " +
        "id=\"dateOfTransaction\"></div>";
    datePicker();

    var descriptionId = document.getElementById('description' + rowNr);
    descriptionId.innerHTML = "<div class=\"form-group\"><input type=\"text\" value=\"" + description + "\" class=\"form-control\" " +
        "id=\"descriptionOfTransaction\"></div>";

    var categoryId = document.getElementById('categoryId' + rowNr);
    categoryId.innerHTML = "<div class=\"form-group\"><select class=\"form-control\" " +
        "id=\"transactionCategory\"></select></div>";
    getAllCategoriesOneSelected(categoryId2, "transactionCategory");

    var buttonCellId = document.getElementById('buttonCell' + rowNr);
    $('#modifyTransactionButton' + rowNr).hide();
    $('#deleteTransactionButton' + rowNr).hide();
    buttonCellId.innerHTML = "<button type=\"button\" onClick='modifyTransactionConfirm(" + transactionId + ", " + userId + ", " + groupId + ", " + rowNr + ")' " +
        "class=\"btn btn-success btn-modify\" id = \"tempButtonMod\">Kinnita</button>" +
        "<button type=\"button\" onClick='confirmDates()' class=\"btn btn-outline-danger btn-delete\" " +
        "id = \"tempButtonCancel\">Loobu</button>";
}

// change data on sql table "transactions"
function modifyTransactionConfirm(transactionId, userId, groupId, rowNr) {
    for (var i=0; i < allTransactions.length; i++) {
        if (allTransactions[i].transactionId == transactionId) {
            //document.getElementById("dateModify").value = allTransactions[i].dateOfTransactionString;
            var modifyTransactionJson = {
                "dateOfTransactionString": document.getElementById("dateOfTransaction").value,
                "amount": parseFloat(document.getElementById("sumOfTransaction").value.replace(",", ".")),
                "description": document.getElementById("descriptionOfTransaction").value,
                "categoryId": parseInt(document.getElementById("transactionCategory").value),
                "transactionId": transactionId,
                "transGroupId": groupId,
                "userId": userId
            };
            var modifyTransaction = JSON.stringify(modifyTransactionJson);

            $.ajax({
                url : "http://localhost:8080/rest/transactions",
                type : "PUT",
                data : modifyTransaction,
                contentType : "application/json; charset=utf-8",
                success : function() {
                    $('#tempButtonMod').hide();
                    $('#tempButtonCancel').hide();
                    confirmDates();
                    $('#modifyTransactionButton' + rowNr).show();
                    $('#deleteTransactionButton' + rowNr).show();
                },
                error : function(XMLHttpRequest, textStatus) {
                    console.log(textStatus);
                }
            });
        }
    }
}