function main() {
    datePicker();

    if($('#transactionCategory').length) {
        getCategoriesOfThisGroup();
    }

    if($('#transaction-category').length) {
        getAllCategories("transaction-category");
    }

    $('#new-category-dismissed').on('click', function () {
        $('.add-category-window').hide();
    });

    $('#category-delete-dismissed').on('click', function () {
        $('.delete-category-window').hide();
    });

    // to display the name of the file that was selected
    $('.custom-file-input').on('change',function(){
        var fileNameArray = $(this).val().split("\\");
        var fileName = fileNameArray[fileNameArray.length - 1];
        $(this).next('.form-control-file').addClass("selected").html(fileName);
    });

// to get the name of the user on all pages
    $.getJSON("http://localhost:8080/rest/users/name", function(user){
        $('#user-name-text').text(user.firstname);
        $('#isLoggedIn').text("Tere, " + user.firstname);
    });


    $.getJSON("http://localhost:8080/rest/patterns", function(patterns){
        var contentOfDiv = document.getElementById("category-rules");
        var contentToAdd = "";
        if (patterns.length < 1) {
            contentToAdd += "<p>Sul ei ole veel ühtegi reeglit tehingute automaatseks kategoriseerimiseks määratud.</p>";
        } else {
            var patternDescriptions = new Map();
            var rules = [];
            for (var j = 0; j < patterns.length; j++) {
                if (patternDescriptions[patterns[j].category.categoryName] === undefined) {
                    rules.push(patterns[j].rule);
                    patternDescriptions[patterns[j].category.categoryName] = rules;
                    rules = [];
                } else {
                    patternDescriptions[patterns[j].category.categoryName].push(patterns[j].rule);
                }
            }
            for (var key in patternDescriptions) {
                if (patternDescriptions[key].length > 1) {
                    var rules = "";
                    for (var k = 0; k < patternDescriptions[key].length; k++) {
                        rules += patternDescriptions[key][k];
                        if (k !== (patternDescriptions[key].length - 1)) {
                            rules += "</b> või <b>";
                        }
                    }
                    contentToAdd += "<p>Kui kande kirjeldus sisaldab sõnu <b>" + rules + "</b>, siis on kategooriaks vaikimisi määratud <b>"
                        + key + "</b>.</p>";
                } else {
                    contentToAdd += "<p>Kui kande kirjeldus sisaldab sõna <b>" + patternDescriptions[key][0] + "</b>, siis on kategooriaks vaikimisi määratud <b>"
                        + key + "</b>.</p>";
                }
            }
        }
        contentOfDiv.innerHTML = contentToAdd;
    });
}

  $(document).ready(main);
var allTransactions;
var isClickedOnAddNewUserButton = false;


$('#addCategoryButton').on('click', function () {
    $('.add-category-window').show();
});

$('#deleteCategoryButton').on('click', function () {
    $('#alert-text-cannot-delete-category').css("display", "none");
    getAllCategories("category-list");
    $('.delete-category-window').show();
});

$('#addRuleButton').on('click', function () {
    $('.add-pattern-window').show();
});

$('#new-pattern-dismissed').on('click', function () {
    $('.add-pattern-window').hide();
});

$('#deleteRuleButton').on('click', function () {
    $('.delete-pattern-window').show();
    $.getJSON("http://localhost:8080/rest/patterns", function(patterns) {
        var contentOfDiv = document.getElementById("pattern-dropdown");
        var contentToAdd = "";
        if (patterns.length > 1) {
            for (var i = 0; i < patterns.length; i++) {
                contentToAdd += "<option value=" + patterns[i].patternId + ">" + patterns[i].rule + " -> " +
                    patterns[i].category.categoryName + "<option>";
            }
            contentOfDiv.innerHTML += contentToAdd;
        }
    });
});

$('#pattern-deleted').on('click', function () {
    deletePattern();
});

$('#pattern-delete-dismissed').on('click', function () {
    $('.delete-pattern-window').hide();
});


// on attempting to delete a category
$('#delete-category-confirm').on('click', function () {
    var categoryId = parseInt($('#category-list').find(":selected").val());
    var urlName = "http://localhost:8080/rest/transactions/byGroup/" + categoryId;       // selecting this group's transactions in that category
    $.getJSON(urlName, function(transactions) {
        if (transactions.length === 0 || transactions === undefined) {
            // remove category link to this group on "group_categories" table
            deleteCategoryFromGroupCategory(categoryId);
        } else {
            // alert the user that this category can't be removed
            $('#alert-text-cannot-delete-category').css("display", "initial");
        }
    });
});

function deleteCategoryFromGroupCategory(categoryId) {
    var deleteCategoryJson = {"categoryId": categoryId};
    $.ajax({
        url: "http://localhost:8080/rest/groupCategories",
        type: "DELETE",
        data: JSON.stringify(deleteCategoryJson),
        contentType: "application/json; charset=utf-8",
        success: function () {
            $('.delete-pattern-window').hide();
            getCategoriesOfThisGroup();
            $('.delete-category-window').hide();
            $('#info-category-deleted').text("Kategooria kustutatud");
            $('#info-category-deleted').css('display', 'initial');
            setTimeout(function() {
                $("#info-category-deleted").css('display', 'none');
            }, 5000);
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}

$('#addSecondCategoryButton').on('click', function () {
    $('.add-category-window').show();
});

$('#reject-requestButton').on('click', function()  {
   $('.confirm-group-join-request-window').css('visibility', 'hidden');
    location.href='account.html';
    getCategoriesOfThisGroup();
});


// validator functions for input fields
(function() {
    'use strict';

    window.addEventListener('load', function() {
        var form = document.getElementById('category-needs-validation');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    addNewCategory();
                    form.classList.remove('was-validated');
                }
            }, false);
        }
    }, false);

    window.addEventListener('load', function() {
        var form = document.getElementById('pattern-needs-validation');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    addNewPattern();
                    form.classList.remove('was-validated');
                }
            }, false);
        }
    }, false);

    window.addEventListener('load', function() {
        var form = document.getElementById('transaction-needs-validation');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false || (document.getElementById("sumOfTransaction").value.replace(",", ".")).match(/[a-z]/i)
                    || document.getElementById("sumOfTransaction").value <= 0) {
                    event.preventDefault();
                    event.stopPropagation();
                    emptyFields();
                    form.classList.add('was-validated');
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    var dateOfTransaction = document.getElementById("dateOfTransaction").value;
                    var amount =  parseFloat(document.getElementById("sumOfTransaction").value.replace(",", "."));
                    var description = document.getElementById("descriptionOfTransaction").value;
                    var categoryId = parseInt(document.getElementById("transaction-category").value);
                    addNewTransaction(dateOfTransaction, amount, description, categoryId);
                    form.classList.remove('was-validated');
                }
            }, false);
        }
    }, false);

    window.addEventListener('load', function() {
        var form = document.getElementById('dates-needs-validation');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    confirmDates();
                    form.classList.remove('was-validated');
                }
            }, false);
        }
    }, false);

    window.addEventListener('load', function() {
        var form = document.getElementById('analysis-dates-needs-validation');
        if(form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    confirmDatesForAnalysis();
                    form.classList.remove('was-validated');
                }
            }, false);
        }
    }, false);
})();

function deletePattern(){
    var patternIdToBeDeleted = parseInt(document.getElementById("pattern-dropdown").value);
    var deletePatternJson = {"patternId": patternIdToBeDeleted};

    $.ajax({
        url: "http://localhost:8080/rest/patterns",
        type: "DELETE",
        data: JSON.stringify(deletePatternJson),
        contentType: "application/json; charset=utf-8",
        success: function () {
            $('.delete-pattern-window').hide();
            location.href='account.html';
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}

// adding a new category. Does not enter a category that is already present.
function addNewCategory() {
    var isOldCategoryToAll = false;
    var thisCategoryId = 0;
    var bool_value = false;
    if (parseInt(document.getElementById("categoryType").value) === 1) {
        bool_value = true;
    }
    $.getJSON("http://localhost:8080/rest/categories", function(allCategories) {
        for (var i = 0; i < allCategories.length; i++) {
            var categoryName = document.getElementById("categoryName").value;
            // checks if category name that was entered by the user is the same as a name in a database entry
            // AND category type (income or expense) is the same as the type in that database entry
            console.log(allCategories[i].categoryName === categoryName
                && allCategories[i].categoryExpense === bool_value);
            if (allCategories[i].categoryName === categoryName
                && allCategories[i].categoryExpense === bool_value) {
                thisCategoryId = allCategories[i].categoryId;
                isOldCategoryToAll = true;
                checkIfCategoryIsLinkedToGroup(categoryName, thisCategoryId);
                break;  // no need to continue the for loop, an old category with this name has been found from "category" table
            }
        }
        // if a new name of a category has been entered, it will be added to the table "category" and linked to this group in "group_category" table
        if (!isOldCategoryToAll) {
            var newCategoryJson = {
                "categoryExpense": bool_value,
                "categoryName": document.getElementById("categoryName").value
            };
            if (newCategoryJson.categoryName !== "") {
                postNewCategory(newCategoryJson);
            }
        }
        $('.add-category-window').hide();
    });
}

// ajax post to sql (=> new pattern)
function addNewPattern() {
    var categoryId = parseInt(document.getElementById("transaction-category").value);
    var rule = document.getElementById("pattern-rule").value;
    var newPatternJson = {
        "categoryId": categoryId,
        "rule": rule
    };
    var newPattern = JSON.stringify(newPatternJson);

    $.ajax({
        url: "http://localhost:8080/rest/patterns",
        type: "POST",
        data: newPattern,
        contentType: "application/json; charset=utf-8",

        success: function (category) {
            $('.add-pattern-window').hide();
            location.href='account.html';
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}

// ajax post to sql (=> new category)
function postNewCategory(newCategoryJson) {
    var newCategory = JSON.stringify(newCategoryJson);

    $.ajax({
        url: "http://localhost:8080/rest/categories",
        type: "POST",
        data: newCategory,
        contentType: "application/json; charset=utf-8",
        success: function (category) {
            // linking to this group on "group_category" table
            addGroupCategory(category.categoryId);
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}

function checkIfCategoryIsLinkedToGroup(categoryName, thisCategoryId) {
    $.getJSON("http://localhost:8080/rest/categories/groupId", function(groupCategories){
        var isOldCategoryToThisGroup = false;
        for(var i = 0; i < groupCategories.length; i++) {
            if (groupCategories[i].categoryName === categoryName) {
                // this group already has this category
                isOldCategoryToThisGroup = true;
                $('#info-category-deleted').text("Sellise nimega kategooria on juba olemas");
                $('#info-category-deleted').css('display', 'initial');
                setTimeout(function() {
                    $("#info-category-deleted").css('display', 'none');
                }, 5000);
                break;  // no need to continue the for loop, this category has been found from connected to this group from "group_category" table
            }
        }
        if (!isOldCategoryToThisGroup) {
            addGroupCategory(thisCategoryId);
        }
    });
}

// adding a category to a group on table "group_category"
function addGroupCategory(categoryId) {
    var newGroupCategoryJson = {
        "categoryId": categoryId
    };
    if (newGroupCategoryJson.categoryId !== "") {
        var newGroupCategory = JSON.stringify(newGroupCategoryJson);

        $.ajax({
            url: "http://localhost:8080/rest/groupCategories",
            type: "POST",
            data: newGroupCategory,
            contentType: "application/json; charset=utf-8",

            success: function (groupCategory) {
                if($('#transactionCategory').length) {
                    getCategoriesOfThisGroup();
                }
                if($('#transaction-category').length) {
                    getAllCategories("transaction-category");
                }
                $('.add-category-window').hide();
            },
            error: function (XMLHttpRequest, textStatus) {
                console.log(textStatus);
            }
        })
    }
}

// for picking dates in Estonian language
function datePicker() {
    $.datepicker.setDefaults($.datepicker.regional['et']);
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
}

function confirmDates() {
    var startDate = document.getElementById("startDateOfTransaction").value;
    var endDate = document.getElementById("endDateOfTransaction").value;
    getAllTransactions(startDate, endDate);
}

function askIfDelete(transactionID) {
    $('.delete-transaction-window').css('display', 'initial');
    var popUp = document.getElementById('pop-up-window');
    popUp.innerHTML = "<p>Kas soovid tehingu kustutada?</p>\n" +
        "        <button type=\"button\" onClick='deleteTransaction(" + transactionID + ")' class=\"btn btn-success\" id=\"deleteConfirmationButton\">Jah</button>\n" +
        "        <button type=\"button\" onClick='closeConfirmationWindow()' class=\"btn btn-outline-secondary\">Ei</button>";
}

function closeConfirmationWindow() {
    $('.delete-transaction-window').css('display', 'none');
}

//displays the list of categories
function getAllCategories(dropDownId) {
    $.getJSON("http://localhost:8080/rest/categories/groupId", function(categories){
        var selectionContent = document.getElementById("" + dropDownId + "");
        var filledContent = "";
        for(var i = 0; i < categories.length; i++) {
            filledContent += "<option value=\"" + categories[i].categoryId + "\">" + categories[i].categoryName + "</option>";
        }
        selectionContent.innerHTML = filledContent;
    });
}

//gets categories of this group
function getCategoriesOfThisGroup() {
    $.getJSON("http://localhost:8080/rest/categories/groupId", function(categories){
        var selectionContent = document.getElementById("transactionCategory");
        var filledContent = "<ul>";
        if (categories !== undefined && categories.length > 0) {
            for(var i = 0; i < categories.length; i++) {
                filledContent += "<li>" + categories[i].categoryName + "</li>";
            }
            filledContent += "</ul>";
            selectionContent.innerHTML = filledContent;
            $('#not-first-login').css('display', 'block');
            $('#first-login-category-choice').css('display', 'none');
            $('#my-rules').css('display', 'block');
        } else {
            $('#first-login-category-choice').css('display', 'block');
        }
    });
}

// at first login user can choose categories he/she wants to use
function getCategoriesFromForm() {
    // retrieve a list of category IDs that the user has selected
    var listOFCategories = [];
    for (var i = 1; i < 15; i++) {
        if ($('#checkbox' + i).is(':checked')) {
            listOFCategories[listOFCategories.length] = parseInt($('#checkbox' + i).val());
        }
    }
    // posts all categories the user has selected
    for (var j = 0; j < listOFCategories.length; j++) {
        var catId = listOFCategories[j];
        var newCategoryJson = {
            "categoryId": catId
        };
        if (newCategoryJson.categoryId !== "") {
            postGroupCategory(newCategoryJson, listOFCategories);
        }
    }
}

function postGroupCategory(newCategoryJson, listOfCategories) {
    var newCategory = JSON.stringify(newCategoryJson);

    $.ajax({
        url: "http://localhost:8080/rest/groupCategories",
        type: "POST",
        data: newCategory,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            if (response.categoryId === listOfCategories[listOfCategories.length - 1]) {
                getCategoriesOfThisGroup();

            }
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}
