
// parsing data from csv input file
$('#read-from-file-button').on('click', function() {
    // Luminor bank .csv files need different encoding
    if ($('#bank-id').find(":selected").val() === "Luminor") {
        $('input[type=file]').parse({
            config: {
                // base config to use for each file
                encoding: "ISO-8859-1",
                complete: function(results, file) {
                    getTransactionsFromCSVFile(results);
                }
            },
            error: function(err, file, inputElem, reason)
            {
                console.log("error parsing");

            }
        });
    } else {
        $('input[type=file]').parse({
            config: {
                // base config to use for each file
                complete: function(results, file) {
                    getTransactionsFromCSVFile(results);
                }
            },
            error: function(err, file, inputElem, reason)
            {
                console.log("error parsing");

            }
        });
    }
});

//interpret data from parsing .csv file
function getTransactionsFromCSVFile(results){
    $('.multiple-trans').css('visibility', 'visible');
    var tableBody = document.getElementById("multipleTransactionsTableBody");
    var data = results.data;
    var tableContent = "";
    var categoriesOfThisGroup = "<option value=-1>...</option>";
    console.log(categoriesOfThisGroup);
    $.getJSON("http://localhost:8080/rest/categories/groupId", function(categories){
        for (var k = 0; k < categories.length; k++) {
            categoriesOfThisGroup += "<option value=" + categories[k].categoryId + ">" + categories[k].categoryName + "</option>";
        }
        if ($('#bank-id').find(":selected").val() === "Swedbank"){
            tableContent += swedbankData(data, categoriesOfThisGroup, tableContent);
        } else if ($('#bank-id').find(":selected").val() === "LHV") {
            tableContent += lhvData(data, categoriesOfThisGroup, tableContent);
        } else if ($('#bank-id').find(":selected").val() === "Luminor") {
            tableContent += luminorData(data, categoriesOfThisGroup, tableContent);
        } else if ($('#bank-id').find(":selected").val() === "SEB") {
            tableContent += sebData (data, categoriesOfThisGroup, tableContent);
        }
        tableBody.innerHTML = tableContent;
        for (var i = 0, row; row = tableBody.rows[i]; i++) {
            var descriptionText = row.cells[3].textContent.trim();
            var rowNr = parseInt(row.cells[0].textContent.trim());
            findPatternInDescription (descriptionText, 'categoryId' + rowNr);
        }
    });
}

// formats data from Swedbank bank .csv file
function swedbankData(data, categoriesOfThisGroup) {
    var amountColumn = 0;
    var descriptionColumn = 0;
    var tableContent = "";
    if (data[0][5] === "Selgitus") {
        descriptionColumn = 5;
        amountColumn = 6;
    } else {
        descriptionColumn = 4;
        amountColumn = 5;
    }
    for (var j = 2; j < data.length - 3; j++) {
        var deleteTransactionButton = 'deleteTransactionButton' + j;
        // format of date and description strings change:
        var dateArray = data[j][2].split(".");
        var dateOtherFormat = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
        var descriptionIndexOfAlpha = data[j][descriptionColumn].search(/[a-z]/i);
        var descriptionIndexOfSlash = data[j][descriptionColumn].indexOf("\\");
        var descriptionSliced;
        if (descriptionIndexOfSlash === -1) {
            descriptionSliced = data[j][descriptionColumn].slice(descriptionIndexOfAlpha);
        } else {
            descriptionSliced = data[j][descriptionColumn].slice(descriptionIndexOfAlpha, descriptionIndexOfSlash);
        }
        var counterParty = data[j][3];
        var amount = data[j][amountColumn];
        tableContent += fillHtmlTableRowWithCsvInfo((j - 1), dateOtherFormat, counterParty, amount, descriptionSliced, categoriesOfThisGroup, deleteTransactionButton);
    }
    return tableContent;
}

// formats data from LHV bank .csv file
function lhvData(data, categoriesOfThisGroup) {
    var tableContent = "";
    for (var m = 1; m < data.length - 1; m++) {
        var deleteTransactionButton = 'deleteTransactionButton' + m;
        // format of date and description strings change:
        var dateOtherFormat = data[m][2];
        var descriptionIndexOfAlpha = data[m][11].search(/[a-z]/i);
        var descriptionIndexOfSlash = data[m][11].indexOf("\\");
        var descriptionSliced;
        if (descriptionIndexOfSlash === -1) {
            descriptionSliced = data[m][11].slice(descriptionIndexOfAlpha);
        } else {
            descriptionSliced = data[m][11].slice(descriptionIndexOfAlpha, descriptionIndexOfSlash);
        }
        var counterParty = data[m][4];
        var amount = Math.abs(data[m][8]);
        tableContent += fillHtmlTableRowWithCsvInfo(m, dateOtherFormat, counterParty, amount, descriptionSliced, categoriesOfThisGroup, deleteTransactionButton);
    }
    return tableContent;
}

// formats data from Luminor bank .csv file
function luminorData(data, categoriesOfThisGroup) {
    var tableContent = "";
    for (var n = 1; n < data.length - 1; n++) {
        var deleteTransactionButton = 'deleteTransactionButton' + n;
        // format of date and description strings change:
        var dateArray = data[n][2].split(".");
        var dateOtherFormat = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
        var descriptionText = data[n][9];
        var isExcessText = false;
        var textBeginning = ['Pangasisene makse', 'Pangasisene laekumine', 'Pangasisene Db tehing', 'Väljaminev euromakse', 'Sissetulev euromakse', 'Kaarditehing']
        var descriptionIndexOfAlpha;
        var descriptionSliced;
        for (var d = 0; d < descriptionText.length; d++) {
            descriptionIndexOfAlpha = descriptionText.indexOf(textBeginning[d]);
            if (descriptionIndexOfAlpha !== -1) {
                descriptionSliced = descriptionText.slice(descriptionIndexOfAlpha + textBeginning[d].length);
                isExcessText = true;
            }
        }
        if (!isExcessText) {
            descriptionSliced = descriptionText;
        }
        var counterParty = data[n][4];
        var amount = data[n][6];
        tableContent += fillHtmlTableRowWithCsvInfo(n, dateOtherFormat, counterParty, amount, descriptionSliced, categoriesOfThisGroup, deleteTransactionButton);
    }
    return tableContent;
}

// formats data from SEB bank .csv file
function sebData (data, categoriesOfThisGroup) {
    var tableContent = "";
    for (var v = 1; v < data.length - 2; v++) {
        var deleteTransactionButton = 'deleteTransactionButton' + v;
        // format of date and description strings change:
        var dateArray = data[v][2].split(".");
        var dateOtherFormat = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
        var descriptionText = data[v][11];
        var descriptionIndexOfAlpha = descriptionText.indexOf("kaart...");
        var descriptionSliced;
        if (descriptionIndexOfAlpha !== -1) {
            descriptionSliced = descriptionText.slice(descriptionIndexOfAlpha + 15);
        } else {
            descriptionSliced = descriptionText;
        }
        var counterParty = data[v][4];
        var amount = data[v][8];
        tableContent += fillHtmlTableRowWithCsvInfo(v, dateOtherFormat, counterParty, amount, descriptionSliced, categoriesOfThisGroup, deleteTransactionButton);
    }
    return tableContent;
}

function fillHtmlTableRowWithCsvInfo(j, dateOtherFormat, counterParty, amount, descriptionSliced, categoriesOfThisGroup, deleteTransactionButton){
    // id-s of table cells
    console.log(j, dateOtherFormat, counterParty, amount, descriptionSliced, categoriesOfThisGroup, deleteTransactionButton);
    var rowId = 'rowId' + j;
    var dateId = 'date' + j;
    var descriptionId = 'description' + j;
    var counterPartyId = 'counterParty' + j;
    var categoryId = 'categoryId' + j;
    var amountId = 'amount' + j;
    var buttonColumnId = 'buttonColumn' + j;

    return "<tr id='" + rowId + "'><td>" + j +
        "</td><td id='" + dateId + "'>" + dateOtherFormat +
        "</td><td id='" + counterPartyId + "'>" + counterParty +
        "</td><td id='" + descriptionId + "'>" + descriptionSliced +
        "</td><td><div class=\"form-group\"><select  id='" + categoryId + "' class=\"form-control categories-dropdown-list\" " +
        ">" + categoriesOfThisGroup + "</select></div>" +
        "</td><td id='" + amountId + "'>" + amount +
        "</td><td id='" + buttonColumnId + "'>" +
        "<button type=\"button\" onClick='askDeleteConfirmation(" + j + ")' class=\"btn btn-outline-danger btn-delete\" " +
        "id = \"" + deleteTransactionButton + "\">X</button>" + "</td></tr>" +
        "</td></tr>";
}

function deleteRowFromCsvInput(rowId) {
    console.log("Kustutame päriselt rea " + rowId);
    $('#buttonColumn' + rowId).closest('tr').remove();
    closeConfirmationWindow();
}

// asks confirmation to delete a row from the table that was generated using .csv file
function askDeleteConfirmation(rowId) {
    $('.delete-transaction-window').css('display', 'initial');
    var popUp = document.getElementById('pop-up-window');
    popUp.innerHTML = "<p>Kas soovid tehingu kustutada?</p>\n" +
        "        <button type=\"button\" onClick='deleteRowFromCsvInput(" + rowId + ")' class=\"btn btn-success\" id=\"deleteConfirmationButton\">Jah</button>" +
        "        <button type=\"button\" onClick='closeConfirmationWindow()' class=\"btn btn-outline-secondary\">Ei</button>";
}

// POST to sql using info on the table that was generated using .csv file
function addMultipleTransactions() {
    var tableBody = document.getElementById("multipleTransactionsTableBody");
    var isNoCategoryChosen = false;
    var categoryId;
    for (var i = 0, row; row = tableBody.rows[i]; i++) {
        //iterate through rows to check if all categories are chosen
        //rows would be accessed using the "row" variable assigned in the for loop
        var rowNr = parseInt(row.cells[0].textContent.trim());
        categoryId = parseInt($('#categoryId' + rowNr).find(":selected").val());
        if (categoryId === -1) {
            console.log("Unmarked category");
            isNoCategoryChosen = true;
            $('#categoryId' + rowNr).css('border-color', 'red');
            $('.adding-transactions-succeeded').text("Mõned kategooriad on valimata!");
            $('.adding-transactions-succeeded').css({'visibility': 'visible', 'color': 'red'});
            setTimeout(function() {
                $(".adding-transactions-succeeded").css('visibility', 'hidden');
            }, 10000);
        }
    }
    if (!isNoCategoryChosen) {
        console.log(isNoCategoryChosen);
        console.log("No unmarked categories");
        for (var j = 0, row2; row2 = tableBody.rows[j]; j++) {
            //iterate through rows to add all transactions
            var rowNr = parseInt(row2.cells[0].textContent.trim());
            var amount = parseFloat(row2.cells[5].textContent.trim().replace(",", "."));
            var dateOfTransaction = row2.cells[1].textContent.trim();
            var description = row2.cells[3].textContent.trim();
            categoryId = parseInt($('#categoryId' + rowNr).find(":selected").val());
            addNewTransaction (dateOfTransaction, amount, description, categoryId);
        }
        $('.multiple-trans').hide();
        $('.adding-transactions-succeeded').text("Kõik tehingud lisatud");
        $('.adding-transactions-succeeded').css({'visibility': 'visible', 'color': 'black'});
        setTimeout(function() {
            $(".adding-transactions-succeeded").css('visibility', 'hidden');
        }, 3000);
    }
}

function findPatternInDescription (descriptionText, dropDownId) {
    var categoryIdToBeSelected;
    $.getJSON("http://localhost:8080/rest/patterns", function(patterns){
        for (var k = 0; k < patterns.length; k++) {
            var rule = patterns[k].rule.toLowerCase();
            var description = descriptionText.toLowerCase();
            if (description.search(rule) >= 0) {
                categoryIdToBeSelected = patterns[k].category.categoryId;
                getAllCategoriesOneSelected(categoryIdToBeSelected, dropDownId);
                break;
            }
        }
    });
}

// empties fields of new transaction addition
function emptyFields() {
    var amount = document.getElementById("sumOfTransaction");
    amount.value = "";
    var date = document.getElementById("dateOfTransaction");
    date.value = "";
    var description = document.getElementById("descriptionOfTransaction");
    description.value = "";
}


// adds info about new transaction
function addNewTransaction (dateOfTransaction, amount, description, categoryId) {
    var newTransactionJson = {
        "dateOfTransactionString": dateOfTransaction,
        "amount": amount,
        "description": description,
        "categoryId": categoryId
    };
    console.log(newTransactionJson);
    var newTransaction = JSON.stringify(newTransactionJson);

    $.ajax({
        url: "http://localhost:8080/rest/transactions/byUser",
        type: "POST",
        data: newTransaction,
        contentType: "application/json; charset=utf-8",
        success: function () {
            emptyFields();  // empties input fields
            // shows info of success and hides it after 2s
            $('#infoField').css('visibility', 'visible');
            setTimeout(function() {
                $("#infoField").css('visibility', 'hidden');
            }, 2000);
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    })
}
