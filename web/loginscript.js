// User sign in and sign out using Google account
function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var email = profile.getEmail();
    var firstName = profile.getGivenName();
    var lastName = profile.getFamilyName();
    var isOldUser = false;
    $('#join-to-group').css('display', 'none');

    $.getJSON("http://localhost:8080/rest/users", function (allUsers) {
        for (var i = 0; i < allUsers.length; i++) {
            if (allUsers[i].email === email) {
                isOldUser = true;
                oldUserSignIn(allUsers[i], email);
            }
        }
        if (!isOldUser) {
            $('#alert-text').text("Palun loo esmalt kasutaja.").css('display', 'initial');
            signOut();
            // New user has entered, needs to be assigned into a group and added to the database
            if (document.getElementById("email-of-group-leader").value === "") {
                // creating new group
                var groupName = document.getElementById("new-group-name").value;
                var newGroupJson = {
                    "groupName": groupName
                };
                if (newGroupJson.groupName !== "" && isClickedOnAddNewUserButton) {
                    createNewGroup(newGroupJson, email, firstName, lastName);
                }
            } else {
                console.log("Grupiliidri email: " + document.getElementById("email-of-group-leader").value);
                addGroupConfirm(document.getElementById("email-of-group-leader").value, email);
                addPotentialUser(firstName, lastName, email);
            }
        }
    });
    $('#create-new-user-window').css('display', 'none');
}

//old user signing in
function oldUserSignIn(oldUser, email) {
    var userGroup = oldUser.groupId;
    var emailOfRequester = oldUser.unconfirmedEmail;

    // check if there is somebody who wants to join this user in his/her group
    if (emailOfRequester !== undefined && emailOfRequester !== "") {
        $('#join-request-text').text("Kas soovid, et inimene e-maili aadressiga " + emailOfRequester
            + " liitub sinu grupiga? Kui valid jah, siis hakkab teie arvepidamine edaspidi olema ühine.");
        $('#confirm-group-join-request-window').css('visibility', 'visible');
        $('.g-signin2').hide();
        $('#enterSystem').hide();
        var divContent = "<form><div class=\"row\">";
        divContent += "<div class=\"col-md-6\"><button type=\"button\" class=\"btn btn-outline-secondary\" id=\"confirm-request\" " +
            "onclick='changePotUserToUser(\"" + emailOfRequester + "\", " + userGroup + ")'>Nõustu</button></div>";
        divContent += "<div class=\"col-md-6\"><button type=\"button\" class=\"btn btn-outline-secondary\" id=\"reject-request\" " +
            "onclick='deletePotentialUser(\"" + emailOfRequester + "\")'>Keeldu</button></div>";
        divContent += "</div></form>";
        $('#confirm-group-join-request-window').append(divContent);
        addGroupConfirm(email, "");     // deletes email of the requester from the group "leader" info on user table
    }
    var loggedInUserJson = {
        "email": email
    };
    if (loggedInUserJson.email !== "") {
        // logging user in - creating user session
        userSessionStart(loggedInUserJson);
    }
    $('#enterSystem').css('visibility', 'visible');     // button to enter Rahaasjad system
    $('#newUser').css('visibility', 'hidden');
}

//creates new group for user and starts the function to enter new user info to user table
function createNewGroup(newGroupJson, email, firstName, lastName) {
    var newGroupId = 0;
    var newGroup = JSON.stringify(newGroupJson);
    // adding a new group
    $.ajax({
        url: "http://localhost:8080/rest/groups",
        type: "POST",
        data: newGroup,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            newGroupId = response.groupId;
            newUserToDatabase(email, firstName, newGroupId, lastName);
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}

//new user info to database table "user"
function newUserToDatabase(email, firstName, newGroupId, lastName) {
    // create new user
    var newUserJson = {
        "email": email,
        "firstname": firstName,
        "groupId": newGroupId,
        "lastname": lastName
    };
    if (newUserJson.email !== "") {
        var newUser = JSON.stringify(newUserJson);

        $.ajax({
            url: "http://localhost:8080/rest/users",
            type: "POST",
            data: newUser,
            contentType: "application/json; charset=utf-8",
            success: function (user) {
                console.log("Uus kasutaja lisatud");
                $('#alert-text').text("Uus kasutajakonto loodud. Palun sisene keskkonda.");
                userSessionStart(user);
            },
            error: function (XMLHttpRequest, textStatus) {
                console.log(textStatus);
            }
        })
    }
}

// logging user in - starting session
function userSessionStart(loggedInUserJson) {
    $.ajax({
        url: "http://localhost:8080/rest/users/login",
        type: "POST",
        data: loggedInUserJson,
        success: function (data) {
            console.log("Success: logged in.");
            $('#enterSystem').css('visibility', 'visible');
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}

// deleting info from potential_user table and adding it to user table
function changePotUserToUser (requesterEmail, userGroup) {
    // gets the info of a new user from "potential_user" table and adds it to "user" table
    $.getJSON("http://localhost:8080/rest/potentialusers/" + requesterEmail, function (user) {
        var requesterEmail = user.email;
        var requesterFirstName = user.firstname;
        var requesterLastName = user.lastname;
        var newUserJson = {
            "email": requesterEmail,
            "firstname": requesterFirstName,
            "groupId": userGroup,
            "lastname": requesterLastName
        };
        if (newUserJson.email !== "") {
            var newUser = JSON.stringify(newUserJson);

            //add user to user table
            $.ajax({
                url: "http://localhost:8080/rest/users",
                type: "POST",
                data: newUser,
                contentType: "application/json; charset=utf-8",
                success: function () {
                    deletePotentialUser(requesterEmail);
                },
                error: function (XMLHttpRequest, textStatus) {
                    console.log(textStatus);
                }
            })
        }
    });
}

// removing info from potential_user table
function deletePotentialUser(requesterEmail) {
    // delete user from potential_user table
    $.ajax({
        url: "http://localhost:8080/rest/potentialusers",
        type: "DELETE",
        data: requesterEmail,
        contentType: "application/json; charset=utf-8",
        success: function () {
            $('.confirm-group-join-request-window').css('visibility', 'hidden');
            location.href='account.html';
            getCategoriesOfThisGroup();
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    });
}

// adding info of this user to potential_user table (can be used later when the request to join group has been accepted)
function addPotentialUser(firstName, lastName, email) {
    console.log("funktsiooni parameetrid: " + firstName + lastName + email);
    var newPotentialUserJson = {
        "email": email,
        "firstname": firstName,
        "lastname": lastName
    };
    var newPotentialUser = JSON.stringify(newPotentialUserJson);
    console.log(newPotentialUserJson);

    $.ajax({
        url: "http://localhost:8080/rest/potentialusers",
        type: "POST",
        data: newPotentialUser,
        contentType: "application/json; charset=utf-8",
        success: function () {
            $('#alert-text').text("Enne kasutajaks liitumist palu, et Sinu liitumistaotlus grupiga aktsepteeritaks (teine kasutaja peab uuesti sisse logima).");
            $('.g-signin2').hide();
        },
        error: function (XMLHttpRequest, textStatus) {
            console.log(textStatus);
        }
    })
}

// adds the email of a new user to the info of an old user on the user table (to make a common group between these users)
function addGroupConfirm(oldUserEmail, newUserEmail) {
    console.log("function to change the details of the user");
    var newUserId = 0;

    $.getJSON("http://localhost:8080/rest/users/" + oldUserEmail, function (user) {
        /*
        if (newUserEmail in user.email) {
            $('#alert-text-no-email-found').text("Sellise aadressiga kasutaja on juba olemas");
            $('#alert-text-no-email-found').css('visibility', 'visible');
        }
        */
        if (user !== undefined) {       // found an old user with the requested email
            var modifyUserJson = {
                "email": user.email,
                "firstname": user.firstname,
                "groupId": user.groupId,
                "lastname": user.lastname,
                "userId": user.userId,
                "unconfirmedEmail": newUserEmail
            };

            console.log(modifyUserJson);
            var modifyUser = JSON.stringify(modifyUserJson);

            // adds the email of a new user to the data of an old user to start forming a group
            $.ajax({
                url: "http://localhost:8080/rest/users/",
                type: "PUT",
                data: modifyUser,
                contentType: "application/json; charset=utf-8",
                success: function () {
                    $('#join-to-group').css('display', 'none');
                    console.log("Success");
                },
                error: function (XMLHttpRequest, textStatus) {
                    console.log(textStatus);
                }
            });
        } else {
            $('#alert-text-no-email-found').css('visibility', 'visible');
            $('#enterSystem').hide();
            $('#alert-text-no-email-found').text("Sellise aadressiga kasutajat ei leitud (gruppi ei saa luua).");
        }
    });
}

// button to enter Rahaasjad account.html page
$('#enterSystem').on('click', function () {
    location.href='account.html';
    getCategoriesOfThisGroup();
});

// closes the window of new user details
$('#new-user-dismissed').on('click', function () {
    $('#create-new-user-window').css('display', 'none');
    $('#newUser').css('display', 'initial');
});

// displays the window of input fields where new user info can be added
$('#newUser').on('click', function () {
    $('#create-new-user-window').css('display', 'initial');
    $('#newUser').css('display', 'none');
    $('.g-signin2').css('display', 'none');
    isClickedOnAddNewUserButton = true;
});

//new user wants to join an existing group
$('#userToExistingGroup').on('click', function () {
    $('#create-new-user-window').css('display', 'none');
    $('#join-to-group').css('display', 'initial');
});

//new user does not want to join an existing group
$('#soloUser').on('click', function () {
    $('#create-new-user-window').css('display', 'none');
    $('.new-group').css('display', 'initial');
});

// signs user out - NOT WORKING
function signOut() {
    $('.g-signin2').show();
    var loggedOutJson = {
        "email": ""
    };
    userSessionStart(loggedOutJson);
    /*   not working
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        var loggedOutJson = {
            "email": ""
        };
        userSessionStart(loggedOutJson);
        console.log('Kasutaja on välja logitud.');
    });
    */
}

// button on logout page to log in again after logout
$('#moveToStart').on('click', function () {
    location.href='index.html';
});

// validator functions for input fields
(function() {
    'use strict';


    window.addEventListener('load', function() {
        var form = document.getElementById('group-name-validation');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    $('.g-signin2').css('display', 'initial');
                    $('#new-group-created').css('display', 'none');
                    form.classList.remove('was-validated');
                }
            }, false);
        }
    }, false);

    window.addEventListener('load', function() {
        var form = document.getElementById('group-leader-email-validation');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    $('.g-signin2').css('display', 'initial');
                    $('#joining-group').css('display', 'none');
                    form.classList.remove('was-validated');
                }
            }, false);
        }
    }, false);
})();