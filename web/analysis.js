// our fancy bar graph
function createBarChart(infoList, tableId) {
    infoList.forEach(function(item, key, mapObj) {
        var cellWidth = 0;
        if (item > 100) {
            cellWidth = "100%";
        } else {
            cellWidth = "" + item + "%";
        }
        $('.columnNr' + key + tableId).css({"background-color": "rgba(250,0,0,0.2)", "width": cellWidth, "font-style": "italic", "color": "rgb(120,120,120"});
    });
}

// generate table(s) to analysis.html page where the info of user's and group's transactions has been calculated by categories
function createAnalysisTable (allGroupCategories, categorySummarized, tableId) {
    var tableBody = document.getElementById(tableId);
    var tableContent = "";
    var incomeSummed = 0;
    var outcomeSummed = 0;
    var counter = 0;
    var infoList = new Map();
    for(var i = 0; i < allGroupCategories.length; i++) {
        for (var j = 0; j < allGroupCategories[i].length; j++) {
            counter += 1;
            var amountOfCategory = 0;
            var percentOfIncome = 0;
            var percentOfInComeString = "";
            var category = allGroupCategories[i][j];
            if (category in categorySummarized) {
                amountOfCategory = categorySummarized[category];
            }
            if (i === 0) {
                incomeSummed += amountOfCategory;
            } else {
                outcomeSummed += amountOfCategory;
                if (incomeSummed !== 0 && amountOfCategory !== 0) {
                    percentOfIncome = ((amountOfCategory * 100/incomeSummed).toFixed(2));
                    percentOfInComeString = percentOfIncome + "%";
                    infoList.set(j, percentOfIncome);
                }
            }
            var rowId= "analysisRowNr" + i;
            var columnId = "columnNr" + j + tableId;
            tableContent += "<tr class='" + rowId + "'><td>" + counter +
                "</td><td>" + category +
                "</td><td class='amountCell'>" + amountOfCategory.toFixed(2) +
                "</td><td><div class='" + columnId + "'>" + percentOfInComeString  + "</div></td></tr>";
        }
    }
    var savedFromIncome = "0";
    if (incomeSummed > 0) {
        savedFromIncome = ((incomeSummed - outcomeSummed)*100/incomeSummed).toFixed(2);
    }
    var summaryBarId = "coloredBar" + tableId;
    var checkNegative = "checkNegative" + tableId;
    tableContent += "<tr class='summaryRow'><td></td><td>Säästetud</td><td class='amountCell' id='" + checkNegative + "'>" + (incomeSummed - outcomeSummed).toFixed(2) +
        "</td><td><div class='" + summaryBarId + "'>" + savedFromIncome  + "%" +
        "</div></td></tr>";
    tableBody.innerHTML = tableContent;
    var savingsPercent = "" + savedFromIncome + "%";
    $('.coloredBar' + tableId).css({'width': savingsPercent, "background-color": "rgba(0,0,250,0.2)", "font-style": "italic", "color": "rgb(120,120,120"});
    if (incomeSummed < outcomeSummed) {
        $('#checkNegative' + tableId).css("background-color", "rgba(250,0,0,0.2)");
    }
    if (infoList.size > 0) {
        createBarChart(infoList, tableId);
    }
}

// retrieves dates from html page
function confirmDatesForAnalysis() {
    var startDate = document.getElementById("startDateOfTransaction").value;
    var endDate = document.getElementById("endDateOfTransaction").value;
    getTransactionsByCategorySummarized(startDate, endDate);
}

// fills the table with transactions' info by category and date
function getTransactionsByCategorySummarized(startDate, endDate) {
    $('.analysis-table-user').css('display', 'none');
    $('.analysis-table-group').css('display', 'none');
    if (endDate < startDate) {
        $('#alert-text-dates').css('visibility', 'visible');
    } else {
        $('#alert-text-dates').css('visibility', 'hidden');
        var allGroupCategories = getGroupCategoriesForAnalysisTable();
        var categorySummarizedByGroup = new Map();
        var categorySummarizedByUser = new Map();

        var urlNameUser = "http://localhost:8080/rest/transactions/byUser/" + startDate + "/" + endDate;       // selecting by userId and startDate/endDate
        $.getJSON(urlNameUser, function(transactions) {
            if (transactions.length === 0 || transactions === undefined) {
                $('#alert-text-dates').text("Antud vahemikus pole Sul tehinguid sisestatud.");
                $('#alert-text-dates').css('visibility', 'visible');
            } else {
                allTransactions = transactions;
                categorySummarizedByUser = addMoneyToCategory(transactions);
                createAnalysisTable(allGroupCategories, categorySummarizedByUser, "analysisTableBodyUser");
                $('.analysis-table-user').css('display', 'initial');
            }
        });

        var urlName = "http://localhost:8080/rest/transactions/" + startDate + "/" + endDate;       // selecting by groupId and startDate/endDate
        $.getJSON(urlName, function(transactions) {
            allTransactions = transactions;
            if (transactions.length !== 0 && transactions !== undefined) {
                categorySummarizedByGroup = addMoneyToCategory(transactions);
                if (categorySummarizedByUser !== categorySummarizedByGroup) {
                    createAnalysisTable(allGroupCategories, categorySummarizedByGroup, "analysisTableBodyGroup");
                    $('.analysis-table-group').css('display', 'initial');
                    $.getJSON("http://localhost:8080/rest/users/name", function(user){
                        var groupNameToDisplay = user.group.groupName;
                        $('#group-name-info').text("Kogu grupi \"" + groupNameToDisplay + "\" tehingute kokkuvõte valitud ajavahemikul:");
                    });
                }
            } else {
                $('#alert-text-dates').text("Antud vahemikus pole tehinguid sisestatud.");
            }
        });
    }
}

// adds amount to category for summary table(s)
function addMoneyToCategory(transactions) {
    var categorySummarized = new Map();
    for (var i = 0; i < transactions.length; i++) {
        var amount = 0;
        var categoryName = transactions[i].category.categoryName;
        amount = parseFloat(transactions[i].amount);
        if (categorySummarized[categoryName] === undefined) {
            categorySummarized[categoryName] = amount;
        } else {
            categorySummarized[categoryName] += amount;
        }
    }
    return categorySummarized;
}

// retrieves all categories of this group for summary table(s)
function getGroupCategoriesForAnalysisTable() {
    var allGroupCategories = [];
    $.getJSON("http://localhost:8080/rest/categories/groupId", function(categories){
        for(var i = 0; i < categories.length; i++) {
            if (!categories[i].categoryExpense) {
                if (allGroupCategories[0] === undefined) {
                    allGroupCategories[0] = [];
                    allGroupCategories[0][0] = categories[i].categoryName;
                } else {
                    allGroupCategories[0][allGroupCategories[0].length] = categories[i].categoryName;
                }
            } else {
                if (allGroupCategories[1] === undefined) {
                    allGroupCategories[1] = [];
                    allGroupCategories[1][0] = categories[i].categoryName;
                } else {
                    allGroupCategories[1][allGroupCategories[1].length] = categories[i].categoryName;
                }
            }
        }
    });
    return allGroupCategories;
}
