package main.java.dao;

public class GroupCategory {
    private int groupCatId;
    private int groupId;
    private int categoryId;
    private Group group;
    private Category category;
    private String categoryName;
    private String groupName;

    public int getGroupCatId() {
        return groupCatId;
    }

    public GroupCategory setGroupCatId(int groupCatId) {
        this.groupCatId = groupCatId;
        return this;
    }

    public int getGroupId() {
        return groupId;
    }

    public GroupCategory setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public GroupCategory setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public Group getGroup() {
        return group;
    }

    public GroupCategory setGroup(Group group) {
        this.group = group;
        this.groupName = group.getGroupName();
        this.groupId = group.getGroupId();
        return this;
    }

    public Category getCategory() {
        return category;
    }

    public GroupCategory setCategory(Category category) {
        this.category = category;
        this.categoryName = category.getCategoryName();
        this.categoryId = category.getCategoryId();
        return this;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public GroupCategory setCategoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    public String getGroupName() {
        return groupName;
    }

    public GroupCategory setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }
}
