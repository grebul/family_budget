package main.java.dao;

public class PotentialUser {
    private int potUserId;
    private String firstname;
    private String lastname;
    private String email;


    public int getPotUserId() {
        return potUserId;
    }

    public PotentialUser setPotUserId(int potUserId) {
        this.potUserId = potUserId;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public PotentialUser setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public PotentialUser setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public PotentialUser setEmail(String email) {
        this.email = email;
        return this;
    }
}
