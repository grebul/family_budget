package main.java.dao;

public class User {
    private int userId;
    private String firstname;
    private String lastname;
    private String email;
    private String username;
    private Group group;
    private int groupId;
    private String groupName;
    private String unconfirmedEmail;

    public Group getGroup() {
        return group;
    }

    public String getUnconfirmedEmail() {
        return unconfirmedEmail;
    }

    public User setUnconfirmedEmail(String unconfirmedEmail) {
        this.unconfirmedEmail = unconfirmedEmail;
        return this;
    }

    public User setGroup(Group group) {
        this.group = group;
        this.groupId = group.getGroupId();
        this.groupName = group.getGroupName();
        return this;

    }

    public int getGroupId() {
        return groupId;
    }

    public User setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }

    public String getGroupName() {
        return groupName;
    }

    public User setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public User setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public User setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public User setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", group_id='" + groupId + '\'' +
                ", unconfirmedEmail='" + unconfirmedEmail + '\'' +
                '}';
    }
}
