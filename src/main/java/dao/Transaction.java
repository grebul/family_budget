package main.java.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction implements Comparable<Transaction> {
    private int transactionId;
    private Date dateOfTransaction;
    private String dateOfTransactionString;
    private String description;
    private float amount;
    private Category category;
    private int categoryId;
    private String categoryName;
    private User user;
    private String userName;
    private int userId;
    private Group group;
    private int transGroupId;
    private String groupName;


    public User getUser() {
        return user;
    }

    public Transaction setUser(User user) {
        this.user = user;
        this.userName = user.getFirstname();
        this.userId = user.getUserId();
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public Transaction setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public Transaction setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Transaction setCategoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    public Category getCategory() {
        return category;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Transaction setCategory(Category category) {
        this.category = category;
        this.categoryId = category.getCategoryId();
        this.categoryName = category.getCategoryName();
        return this;
    }

    public String getDateOfTransactionString() {
        return dateOfTransactionString;
    }

    public Transaction setDateOfTransactionString(String dateOfTransactionString) throws ParseException {
        this.dateOfTransactionString = dateOfTransactionString;
        this.dateOfTransaction = new SimpleDateFormat("yyyy-MM-dd").parse(dateOfTransactionString);
        System.out.println(dateOfTransaction);
        return this;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public Transaction setTransactionId(int transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public Date getDateOfTransaction() {
        return dateOfTransaction;
    }

    public Transaction setDateOfTransaction(Date dateOfTransaction) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        this.dateOfTransactionString = df.format(dateOfTransaction);
        this.dateOfTransaction = dateOfTransaction;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Transaction setDescription(String description) {
        this.description = description;
        return this;
    }

    public float getAmount() {
        return amount;
    }

    public Transaction setAmount(float amount) {
        this.amount = amount;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Transaction setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public Group getGroup() {
        return group;
    }

    public Transaction setGroup(Group group) {
        this.group = group;
        this.groupName = group.getGroupName();
        this.transGroupId = group.getGroupId();
        return this;
    }

    public int getTransGroupId() {
        return transGroupId;
    }

    public Transaction setTransGroupId(int transGroupId) {
        this.transGroupId = transGroupId;
        return this;
    }

    public String getGroupName() {
        return groupName;
    }

    public Transaction setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId=" + transactionId +
                ", dateOfTransaction=" + dateOfTransaction +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", category=" + category +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (transactionId != that.transactionId) return false;
        if (Float.compare(that.amount, amount) != 0) return false;
        if (category != that.category) return false;
        if (userId != that.userId) return false;
        if (dateOfTransaction != null ? !dateOfTransaction.equals(that.dateOfTransaction) : that.dateOfTransaction != null)
            return false;
        if (dateOfTransactionString != null ? !dateOfTransactionString.equals(that.dateOfTransactionString) : that.dateOfTransactionString != null)
            return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = transactionId;
        result = 31 * result + (dateOfTransaction != null ? dateOfTransaction.hashCode() : 0);
        result = 31 * result + (dateOfTransactionString != null ? dateOfTransactionString.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (amount != +0.0f ? Float.floatToIntBits(amount) : 0);
        result = 31 * result + (category !=null ? category.hashCode() : 0);
        result = 31 * result + userId;
        return result;
    }

    @Override
    public int compareTo(Transaction other) {
        if (this.equals(other)) {
            return 0;
        } else if (dateOfTransactionString.compareTo(other.dateOfTransactionString) == 0) {
            if (category.equals(other.category)) {
                if (amount < other.amount) {
                    return -1;
                } else {
                    return +1;
                }
            } else  if (category.compareTo(other.category) < 0) {
                return -1;
            } else {
                return +1;
            }

        } else {
            return dateOfTransactionString.compareTo(other.dateOfTransactionString);
        }

    }
}
