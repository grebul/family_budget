package main.java.dao;

public class Pattern implements Comparable<Pattern> {
    private int patternId;
    private int userId;
    private int categoryId;
    private String rule;
    private User user;
    private Category category;
    private String categoryName;

    public int getPatternId() {
        return patternId;
    }

    public Pattern setPatternId(int patternId) {
        this.patternId = patternId;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Pattern setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Pattern setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getRule() {
        return rule;
    }

    public Pattern setRule(String rule) {
        this.rule = rule;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Pattern setUser(User user) {
        this.user = user;
        this.userId = user.getGroupId();
        return this;
    }

    public Category getCategory() {
        return category;
    }

    public Pattern setCategory(Category category) {
        this.category = category;
        this.categoryId = category.getCategoryId();
        this.categoryName = category.getCategoryName();
        return this;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Pattern setCategoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    @Override
    public String toString() {
        return "Pattern{" +
                "patternId=" + patternId +
                ", userId=" + userId +
                ", categoryId=" + categoryId +
                ", rule='" + rule + '\'' +
                ", user=" + user +
                ", category=" + category +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pattern pattern = (Pattern) o;

        if (patternId != pattern.patternId) return false;
        if (userId != pattern.userId) return false;
        if (categoryId != pattern.categoryId) return false;
        if (!rule.equals(pattern.rule)) return false;
        if (!user.equals(pattern.user)) return false;
        if (!category.equals(pattern.category)) return false;
        return categoryName.equals(pattern.categoryName);
    }

    @Override
    public int hashCode() {
        int result = patternId;
        result = 31 * result + userId;
        result = 31 * result + categoryId;
        result = 31 * result + rule.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + category.hashCode();
        result = 31 * result + categoryName.hashCode();
        return result;
    }

    @Override
    public int compareTo(Pattern other) {
        if (this.equals(other)) {
            return 0;
        } else {
            return rule.compareTo(other.rule);
        }
    }
}
