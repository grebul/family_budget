package main.java.dao;

public class Category implements Comparable<Category> {
    private int categoryId;
    private String categoryName;
    private boolean categoryExpense;

    public int getCategoryId() {
        return categoryId;
    }

    public Category setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public boolean isCategoryExpense() {
        return categoryExpense;
    }

    public Category setCategoryExpense(boolean categoryExpense) {
        this.categoryExpense = categoryExpense;
        return this;
    }

    public Category setCategoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", categoryExpense='" + categoryExpense + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (categoryId != category.categoryId) return false;
        if (categoryExpense != category.categoryExpense) return false;
        return categoryName.equals(category.categoryName);
    }

    @Override
    public int hashCode() {
        int result = categoryId;
        result = 31 * result + categoryName.hashCode();
        result = 31 * result + (categoryExpense ? 1 : 0);
        return result;
    }

    @Override
    public int compareTo(Category other) {
        if (this.equals(other)) {
            return 0;
        } else if (categoryId < other.categoryId){
            return -1;
        } else {
            return +1;
        }
    }
}
