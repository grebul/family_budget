package main.java.resource;

import main.java.dao.Group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class GroupResource {

    public static Group getGroupById(int groupId) {
        String sqlQuery = "SELECT * FROM family_budget.group WHERE group_id=?;";
        Group group = null;
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, groupId);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                group = new Group().setGroupId(results.getInt("group_id"))
                        .setGroupName(results.getString("group_name"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting group set: " + e.getMessage());
        }
        return group;
    }

    public static Group addGroup(Group groups) {
        String sqlQuery = "INSERT INTO family_budget.group (group_name) VALUES (?);";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, groups.getGroupName());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while(resultSet.next()) {
                groups.setGroupId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("Error while adding new group: " + e.getMessage());
        }
        return groups;
    }

    public static Set<Group> getAllGroups() {
        Set<Group> groups = new HashSet<>();
        String sqlQuery = "SELECT * FROM family_budget.group";
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                groups.add(new Group()
                        .setGroupId(results.getInt("group_id"))
                        .setGroupName(results.getString("group_name")));
            }
        } catch (SQLException e) {
            System.out.println("Error getting group  set: " + e.getMessage());
        }
        return groups;
    }
}
