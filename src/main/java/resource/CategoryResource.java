package main.java.resource;

import main.java.dao.Category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;


public abstract class CategoryResource {

    public static Set<Category> getAllCategories() {
        Set<Category> categories = new TreeSet<>();
        String sqlQuery = "SELECT * FROM family_budget.category;";
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                Category category = new Category()
                        .setCategoryId(results.getInt("category_id"))
                        .setCategoryName(results.getString("category_name"))
                        .setCategoryExpense(results.getBoolean("category_expense"));

                categories.add(category);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting category set: " + e.getMessage());
        }
        return categories;
    }

    public static Set<Category> getAllCategoriesByGroupId(int groupId) {
        Set<Category> categories = new TreeSet<>();
        String sqlQuery = "SELECT a.category_id AS category_id, b.category_name, b.category_expense\n" +
                "FROM family_budget.group_category AS a\n" +
                "LEFT JOIN family_budget.category AS b ON a.category_id = b.category_id\n" +
                "WHERE group_id=?\n" +
                "ORDER BY category_expense, category_id;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, groupId);
            ResultSet results = statement.executeQuery();
            Category category;
            while (results.next()) {
                category = new Category().setCategoryId(results.getInt("category_id"))
                        .setCategoryName(results.getString("category_name"))
                        .setCategoryExpense(results.getBoolean("category_expense"));
                categories.add(category);
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting group category list: " + var4.getMessage());
        }
        return categories;
    }


    public static Category addCategory(Category category) {
        System.out.println(category);
        String sqlQuery = "INSERT INTO family_budget.category (category_name, category_expense) VALUES (?, ?);";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, category.getCategoryName());
            statement.setBoolean(2, category.isCategoryExpense());
            statement.executeUpdate();
            System.out.println(sqlQuery);
            ResultSet resultSet = statement.getGeneratedKeys();
            while(resultSet.next()) {
                category.setCategoryId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("Error on adding category: " + e.getMessage());
        }
        System.out.println(category);
        return category;
    }


    public static Category getCategoryById(int categoryId) {
        String sqlQuery = "SELECT * FROM family_budget.category WHERE category_id=?;";
        Category category = null;

        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, categoryId);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                category = new Category().setCategoryId(results.getInt("category_id"))
                                            .setCategoryName(results.getString("category_name"))
                                            .setCategoryExpense(results.getBoolean("category_expense"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting category set: " + e.getMessage());
        }
        return category;
    }

}
