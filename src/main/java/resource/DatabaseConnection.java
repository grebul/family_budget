package main.java.resource;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {
    private static Connection connection =  null;

    public static Connection getConnection() {
        String dbUrl = "jdbc:mysql://localhost:3306/family_budget";
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", "root"); // kasutaja, kellel on õigus andmebaasi sisse logida
        connectionProperties.put("password", "tahvlitPole4");
        loadDriver(); //see tuleb serveril käivitamisel tagasi sisse kommenteerida
        try {
            connection = DriverManager.getConnection(dbUrl, connectionProperties);
        } catch (SQLException e) {
            System.out.println("Error on creating database connection: " +  e.getMessage());
        }
        return connection;
    }

    private static void loadDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
            System.out.println("Error on loading driver: " +  e.getMessage());

            e.printStackTrace();
        }
    }

    public static void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Error on creating database connection: " +  e.getMessage());
        }

    }
}
