package main.java.resource;

import main.java.dao.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;


public abstract class UserResource {


    public static Set<User> getAllUsers() {
        Set<User> users = new HashSet<>();
        String sqlQuery = "SELECT * FROM family_budget.user";
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                users.add(new User()
                        .setUserId(results.getInt("user_id"))
                        .setEmail(results.getString("email"))
                        .setFirstname(results.getString("firstname"))
                        .setLastname(results.getString("lastname"))
                        .setUnconfirmedEmail(results.getString("unconfirmed_email"))
                        .setGroup(GroupResource.getGroupById(results.getInt("group_id"))));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting user set: " + e.getMessage());
        }
        return users;
    }

    public static void deleteUser(User users) {
        String sqlQuery = "DELETE FROM family_budget.user WHERE user_id=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, users.getUserId());
            Integer code = statement.executeUpdate();
            if (code.intValue() != 1) {
                throw new SQLException("Something went wrong on deleting user");
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting user set: " + var4.getMessage());
        }
    }

    public static User addUser(User users) {
        String sqlQuery = "INSERT INTO family_budget.user (firstname, lastname, email, group_id) " +
                "VALUES (?, ?, ?, ?);";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, users.getFirstname());
            statement.setString(2, users.getLastname());
            statement.setString(3, users.getEmail());
            statement.setInt(4, users.getGroupId());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while(resultSet.next()) {
                users.setUserId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("Error on adding user: " + e.getMessage());
        }
        return users;
    }

    public static User getUserById(int userId) {
        User user = null;
        String sqlQuery = "SELECT * FROM family_budget.user WHERE user_id=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, userId);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                user = new User().setFirstname(results.getString("firstname"))
                                    .setUserId(results.getInt("user_id"))
                                    .setEmail(results.getString("email"))
                                    .setLastname(results.getString("lastname"))
                                    .setUnconfirmedEmail(results.getString("unconfirmed_email"))
                                    .setGroupId(results.getInt("group_id"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting user set: " + e.getMessage());
        }
        return user;
    }

    public static User getUserByEmail(String email) {
        User user = null;
        String sqlQuery = "SELECT * FROM family_budget.user WHERE email=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setString(1, email);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                user = new User().setFirstname(results.getString("firstname"))
                        .setUserId(results.getInt("user_id"))
                        .setEmail(results.getString("email"))
                        .setLastname(results.getString("lastname"))
                        .setUnconfirmedEmail(results.getString("unconfirmed_email"))
                        .setGroup(GroupResource.getGroupById(results.getInt("group_id")));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting user set: " + e.getMessage());
        }
        return user;
    }

    public static User updateUser(User users) {
        String sqlQuery = "UPDATE family_budget.user SET firstname=?, lastname=?, email=?, unconfirmed_email=?, group_id=? WHERE email=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setString(1, users.getFirstname());
            statement.setString(2, users.getLastname());
            statement.setString(3, users.getEmail());
            statement.setString(4, users.getUnconfirmedEmail());
            statement.setInt(5, users.getGroupId());
            statement.setString(6, users.getEmail());
            Integer code = statement.executeUpdate();
            if (code.intValue() != 1) {
                throw new SQLException("Something went wrong on updating user");
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting user set: " + var4.getMessage());
        }
        return users;
    }

}

