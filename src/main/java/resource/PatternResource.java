package main.java.resource;

import main.java.dao.Pattern;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.TreeSet;

public class PatternResource {

    public static void deletePattern(Pattern pattern, int userId) {
        String sqlQuery = "DELETE FROM family_budget.pattern WHERE user_id=? AND pattern_id=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, userId);
            statement.setInt(2, pattern.getPatternId());
            Integer code = statement.executeUpdate();
            if (code.intValue() != 1) {
                throw new SQLException("Something went wrong on deleting pattern");
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting pattern: " + var4.getMessage());
        }

    }

    public static Pattern addPattern(Pattern patterns, int userId) {
        String sqlQuery = "INSERT INTO family_budget.pattern (user_id, rule, category_id) " +
                "VALUES (?, ?, ?);";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, userId);
            statement.setString(2, patterns.getRule());
            statement.setInt(3, patterns.getCategoryId());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while(resultSet.next()) {
                patterns.setPatternId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("Error on adding pattern: " + e.getMessage());
        }
        return patterns;
    }

    public static Set<Pattern> getPatternsByUserId(int userId) {
        Set<Pattern> patterns = new TreeSet<>();
        String sqlQuery = "SELECT * FROM family_budget.pattern WHERE user_id=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, userId);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                Pattern pattern = new Pattern().setPatternId(results.getInt("pattern_id"))
                        .setUser(UserResource.getUserById(results.getInt("user_id")))
                        .setRule(results.getString("rule"))
                        .setCategory(CategoryResource.getCategoryById(results.getInt("category_id")));
                patterns.add(pattern);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting pattern set: " + e.getMessage());
        }
        return patterns;
    }
}
