package main.java.resource;

import main.java.dao.Transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public abstract class TransactionResource {

    public static Set<Transaction> getAllTransactions() {
        Set<Transaction> transactions = new TreeSet<>();
        String sqlQuery = "SELECT * FROM family_budget.transaction";
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                transactions.add(new Transaction()
                        .setDateOfTransaction(results.getDate("date_of_transaction"))
                        .setDescription(results.getString("description"))
                        .setAmount(results.getFloat("amount"))
                        .setTransactionId(results.getInt("transaction_id"))
                        .setCategory(CategoryResource.getCategoryById(results.getInt("category_id")))
                        .setUser(UserResource.getUserById(results.getInt("user_id")))
                        .setGroup(GroupResource.getGroupById(results.getInt("group_id"))));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting transaction set: " + e.getMessage());
        }
        return transactions;

    }

    public static void updateTransaction(Transaction transactions, int userId, int groupId) {
        String sqlQuery = "UPDATE family_budget.transaction SET date_of_transaction=?, description=?, amount=?, user_id=?, category_id=?, group_id=? " +
                "WHERE transaction_id = ?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setString(1, transactions.getDateOfTransactionString());
            statement.setString(2, transactions.getDescription());
            statement.setFloat(3, transactions.getAmount());
            statement.setInt(4, userId);
            statement.setInt(5, transactions.getCategoryId());
            statement.setInt(6, groupId);
            statement.setInt(7, transactions.getTransactionId());
            Integer code = statement.executeUpdate();
            if (code.intValue() != 1) {
                throw new SQLException("Something went wrong on updating transaction");
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting transaction set: " + var4.getMessage());
        }

    }

    public static void deleteTransaction(Transaction transactions, int userId) {
        String sqlQuery = "DELETE FROM transaction WHERE transaction_id=? AND user_id=?";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)){
            statement.setInt(1, transactions.getTransactionId());
            statement.setInt(2, userId);
            Integer code = statement.executeUpdate();
            if (code.intValue() != 1) {
                throw new SQLException("Something went wrong on deleting transaction");
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting transaction set: " + var4.getMessage());
        }

    }


    public static Transaction addTransactionByUser(int userId, int groupId, Transaction transactions) {
        String sqlQuery = "INSERT INTO family_budget.transaction (date_of_transaction, description, amount, user_id, category_id, group_id) " +
                "VALUES (?, ?, ?, ?, ?, ?);";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, transactions.getDateOfTransactionString());
            statement.setString(2, transactions.getDescription());
            statement.setFloat(3, transactions.getAmount());
            statement.setInt(4, userId);
            statement.setInt(5, transactions.getCategoryId());
            statement.setInt(6, groupId);
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while(resultSet.next()) {
                transactions.setTransactionId(resultSet.getInt(1));
                }
            } catch (SQLException e) {
            System.out.println("Error on adding transaction: " + e.getMessage());
        }
        return transactions;
    }

    public static Set<Transaction> getTransactionsByGroupAndDate(int groupId, String startDate, String endDate) {
        Set<Transaction> transactions = new TreeSet<>();
        String sqlQuery = "SELECT * FROM family_budget.transaction WHERE group_id = ? AND date_of_transaction BETWEEN ? AND ?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, groupId);
            statement.setString(2, startDate);
            statement.setString(3, endDate);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                Transaction transaction = new Transaction()
                        .setDateOfTransaction(results.getDate("date_of_transaction"))
                        .setDescription(results.getString("description"))
                        .setAmount(results.getFloat("amount"))
                        .setTransactionId(results.getInt("transaction_id"))
                        .setUser(UserResource.getUserById(results.getInt("user_id")))
                        .setCategory(CategoryResource.getCategoryById(results.getInt("category_id")))
                        .setGroup(GroupResource.getGroupById(results.getInt("group_id")));

                transactions.add(transaction);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting transaction set: " + e.getMessage());
        }

        return transactions;
    }

    public static Set<Transaction> getTransactionsByUserAndDate(int userId, String startDate, String endDate) {
        Set<Transaction> transactions = new TreeSet<>();
        String sqlQuery = "SELECT * FROM family_budget.transaction WHERE user_id = ? AND date_of_transaction BETWEEN ? AND ?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, userId);
            statement.setString(2, startDate);
            statement.setString(3, endDate);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                Transaction transaction = new Transaction()
                        .setDateOfTransaction(results.getDate("date_of_transaction"))
                        .setDescription(results.getString("description"))
                        .setAmount(results.getFloat("amount"))
                        .setTransactionId(results.getInt("transaction_id"))
                        .setUser(UserResource.getUserById(results.getInt("user_id")))
                        .setCategory(CategoryResource.getCategoryById(results.getInt("category_id")))
                        .setGroup(GroupResource.getGroupById(results.getInt("group_id")));
                transactions.add(transaction);
            }
        } catch (SQLException e) {
            System.out.println("Error getting transaction set: " + e.getMessage());
        }
        return transactions;
    }

    public static Set<Transaction> getTransactionsByGroupAndCategoryId(int groupId, int categoryId) {
        Set<Transaction> transactions = new TreeSet<>();
        String sqlQuery = "SELECT * FROM family_budget.transaction WHERE group_id = ? AND category_id = ?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, groupId);
            statement.setInt(2, categoryId);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                Transaction transaction = new Transaction()
                        .setDateOfTransaction(results.getDate("date_of_transaction"))
                        .setDescription(results.getString("description"))
                        .setAmount(results.getFloat("amount"))
                        .setTransactionId(results.getInt("transaction_id"))
                        .setUser(UserResource.getUserById(results.getInt("user_id")))
                        .setCategory(CategoryResource.getCategoryById(results.getInt("category_id")))
                        .setGroup(GroupResource.getGroupById(results.getInt("group_id")));
                transactions.add(transaction);
            }
        } catch (SQLException e) {
            System.out.println("Error getting transaction set: " + e.getMessage());
        }
        return transactions;
    }

}
