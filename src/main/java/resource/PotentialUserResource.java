package main.java.resource;

import main.java.dao.PotentialUser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PotentialUserResource {

    public static void deletePotentialUser(String email) {
        String sqlQuery = "DELETE FROM family_budget.potential_user WHERE email=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setString(1, email);
            Integer code = statement.executeUpdate();
            if (code.intValue() != 1) {
                throw new SQLException("Something went wrong on deleting potential user");
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting potential user set: " + var4.getMessage());
        }

    }

    public static PotentialUser addUser(PotentialUser users) {
        String sqlQuery = "INSERT INTO family_budget.potential_user (firstname, lastname, email) " +
                "VALUES (?, ?, ?);";
        //LAST_INSERT_ID() does not work here
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, users.getFirstname());
            statement.setString(2, users.getLastname());
            statement.setString(3, users.getEmail());
            System.out.println("sqlQuery: " + sqlQuery);
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while(resultSet.next()) {
                users.setPotUserId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("Error on adding user: " + e.getMessage());
        }
        return users;
    }


    public static PotentialUser getUserByEmail(String email) {
        PotentialUser user = null;
        String sqlQuery = "SELECT * FROM family_budget.potential_user WHERE email=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setString(1, email);
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                user = new PotentialUser().setFirstname(results.getString("firstname"))
                        .setEmail(results.getString("email"))
                        .setLastname(results.getString("lastname"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting potential user set: " + e.getMessage());
        }
        return user;
    }

}
