package main.java.resource;

import main.java.dao.GroupCategory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GroupCategoryResource {

    public static GroupCategory addGroupCategory(GroupCategory groupCategory, int groupId) {
        String sqlQuery = "INSERT INTO family_budget.group_category (group_id, category_id) VALUES (?, ?);";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, groupId);
            statement.setInt(2, groupCategory.getCategoryId());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            while(resultSet.next()) {
                groupCategory.setGroupCatId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("Error on adding category to group: " + e.getMessage());
        }
        return groupCategory;
    }

    public static void deleteCategoryFromGroup(GroupCategory groupCategory, int groupId) {
        String sqlQuery = "DELETE FROM family_budget.group_category WHERE group_id=? AND category_id=?;";
        try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
            statement.setInt(1, groupId);
            statement.setInt(2, groupCategory.getCategoryId());
            Integer code = statement.executeUpdate();
            if (code.intValue() != 1) {
                throw new SQLException("Something went wrong on deleting category from group_category");
            }
        } catch (SQLException var4) {
            System.out.println("Error on getting category from group_category: " + var4.getMessage());
        }

    }
}
