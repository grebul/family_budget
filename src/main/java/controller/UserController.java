package main.java.controller;

import main.java.dao.User;
import main.java.resource.UserResource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/users")
public class UserController extends BaseController {
    @Context
    private HttpServletRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<User> getAllUsers() {
        return UserResource.getAllUsers();
    }

    @GET
    @Path("/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserByEmail(@PathParam("email") String email) {
        return UserResource.getUserByEmail(email);
    }

    @GET
    @Path("/name")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser() {
        if (checkIfUserLoggedIn()) {
            System.out.println("Getting name: " + getAuthenticatedUser().getFirstname());
            return getAuthenticatedUser();
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @POST
    @Path("/login")
    public boolean userToSession(@FormParam("email") String email) {
        boolean retVal = false;
        User authenticatedUser = UserResource.getUserByEmail(email);
        if (authenticatedUser != null) {
            HttpSession session = request.getSession(true);
            session.setAttribute("user", authenticatedUser);
            retVal = true;
        } else if(email.equals("")) {
            retVal = false;
        }
        return retVal;
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User addUser(User user) {
        return UserResource.addUser(user);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User updateUser(User user) {
        return UserResource.updateUser(user);
    }
}
