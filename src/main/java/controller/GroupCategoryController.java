package main.java.controller;

import main.java.dao.GroupCategory;
import main.java.dao.User;
import main.java.resource.GroupCategoryResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/groupCategories")
public class GroupCategoryController extends BaseController{
    private User authenticatedUser;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public GroupCategory addCategory(GroupCategory groupCategory) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisGroupId = authenticatedUser.getGroupId();
            return GroupCategoryResource.addGroupCategory(groupCategory, thisGroupId);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @DELETE
    public void deleteGroupCategory(GroupCategory groupCategory) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisGroupId = authenticatedUser.getGroupId();
            GroupCategoryResource.deleteCategoryFromGroup(groupCategory, thisGroupId);
        } else {
            System.out.println("User not logged in");
        }
    }

}
