package main.java.controller;


import main.java.dao.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;


public class BaseController {
    public User authenticatedUser = null;

    @Context
    private HttpServletRequest request;

    public boolean checkIfUserLoggedIn() {
        boolean retVal = false;
        HttpSession session = request.getSession (true);
        authenticatedUser = (User) session.getAttribute("user");

        if (authenticatedUser!=null) {
            retVal = true;
        } else {
            System.out.println("ERROR: unauthenticated user");
        }
        return retVal;
    }

    public User getAuthenticatedUser() {
        return authenticatedUser;
    }
}