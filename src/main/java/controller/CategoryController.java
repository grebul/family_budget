package main.java.controller;

import main.java.dao.Category;
import main.java.dao.User;
import main.java.resource.CategoryResource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/categories")

public class CategoryController extends BaseController {
private User authenticatedUser;

    @GET
    @Produces(MediaType.APPLICATION_JSON)   // produces JSON
    public Set<Category> getAllCategories() {
        return CategoryResource.getAllCategories();
    }

    @GET
    @Path("/groupId")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Category> getAllCategoriesByGroupId(){    // limiting categories
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisGroupId = authenticatedUser.getGroupId();
            return CategoryResource.getAllCategoriesByGroupId(thisGroupId);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Category addCategory(Category category) {
        if (checkIfUserLoggedIn()) {
            return CategoryResource.addCategory(category);
        } else {
        System.out.println("User not logged in");
        return null;
        }
    }
}





