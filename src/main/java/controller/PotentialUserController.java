package main.java.controller;

import main.java.dao.PotentialUser;
import main.java.resource.PotentialUserResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/potentialusers")
public class PotentialUserController {


    @GET
    @Path("/{email}")
    @Produces(MediaType.APPLICATION_JSON)   // produces JSON
    public PotentialUser getUserByEmail(@PathParam("email") String email) {
        return PotentialUserResource.getUserByEmail(email);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PotentialUser addPotentialUser(PotentialUser user) {
        System.out.println(user);
        return PotentialUserResource.addUser(user);
    }

    @DELETE
    public void deletePotentialUser(String email) {
        PotentialUserResource.deletePotentialUser(email);
    }
}
