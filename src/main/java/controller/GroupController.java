package main.java.controller;

import main.java.dao.Group;
import main.java.resource.GroupResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/groups")
public class GroupController {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Group addGroup(Group group) {
        return GroupResource.addGroup(group);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)   // produces JSON
    public Set<Group> getAllGroups() {
        return GroupResource.getAllGroups();
    }
}
