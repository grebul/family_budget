package main.java.controller;

import main.java.dao.Pattern;
import main.java.dao.User;
import main.java.resource.PatternResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;


@Path("/patterns")
public class PatternController extends BaseController {
    private User authenticatedUser;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pattern addPattern(Pattern pattern) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisUserId = authenticatedUser.getUserId();
            return PatternResource.addPattern(pattern, thisUserId);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Pattern> getAllPatternsByUserId(){    // limiting categories
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisUserId = authenticatedUser.getUserId();
            return PatternResource.getPatternsByUserId(thisUserId);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @DELETE
    public void deletePattern(Pattern pattern) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisUserId = authenticatedUser.getUserId();
            PatternResource.deletePattern(pattern, thisUserId);
        } else {
            System.out.println("User not logged in");
        }
    }
}

