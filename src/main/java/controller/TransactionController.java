package main.java.controller;

import main.java.dao.Transaction;
import main.java.dao.User;
import main.java.resource.TransactionResource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/transactions")
public class TransactionController extends BaseController {
    private User authenticatedUser;

    @GET
    @Produces(MediaType.APPLICATION_JSON)   // produces JSON
    public Set<Transaction> getAllTransactions() {
        if (checkIfUserLoggedIn()) {
            return TransactionResource.getAllTransactions();
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @GET
    @Path("/{transactionStartDate}/{transactionEndDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Transaction> getAllTransactionsOfGroupByDate(@PathParam("transactionStartDate") String start,
                                                     @PathParam("transactionEndDate") String end) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisGroupId = authenticatedUser.getGroupId();
            return TransactionResource.getTransactionsByGroupAndDate(thisGroupId, start, end);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @GET
    @Path("/byUser/{transactionStartDate}/{transactionEndDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Transaction> getAllTransactionsOfUserByDate(@PathParam("transactionStartDate") String start,
                                                     @PathParam("transactionEndDate") String end) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisUserId = authenticatedUser.getUserId();
            return TransactionResource.getTransactionsByUserAndDate(thisUserId, start, end);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    @GET
    @Path("/byGroup/{categoryId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Transaction> getAllTransactionsOfGroupByCategory(@PathParam("categoryId") int categoryId) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisGroupId = authenticatedUser.getGroupId();
            return TransactionResource.getTransactionsByGroupAndCategoryId(thisGroupId, categoryId);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    // add
    @POST
    @Path("/byUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Transaction addTransactionByUser(Transaction transaction) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisUserId = authenticatedUser.getUserId();
            int thisGroupId = authenticatedUser.getGroupId();
            return TransactionResource.addTransactionByUser(thisUserId, thisGroupId, transaction);
        } else {
            System.out.println("User not logged in");
            return null;
        }
    }

    // change
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateTransaction(Transaction transaction) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisUserId = authenticatedUser.getUserId();
            int thisGroupId = authenticatedUser.getGroupId();
            TransactionResource.updateTransaction(transaction, thisUserId, thisGroupId);
        } else {
            System.out.println("User not logged in");
        }
    }

    @DELETE
    public void deleteTransaction(Transaction transaction) {
        if (checkIfUserLoggedIn()) {
            authenticatedUser = getAuthenticatedUser();
            int thisUserId = authenticatedUser.getUserId();
            TransactionResource.deleteTransaction(transaction, thisUserId);
        } else {
            System.out.println("User not logged in");
        }
    }
}
