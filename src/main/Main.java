/*
package main;

import main.java.dao.Transaction;
import main.java.resource.TransactionResource;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws ParseException {
        //UserResource userResource= new UserResource();
        //Map<Integer, List<String>> users = userResource.getAllUsers();
        //System.out.println("---Kõik kasutajad---");
        //System.out.println(users);


        TransactionResource resource = new TransactionResource();
        Set<Transaction> transactionsSet = resource.getAllTransactions();
        System.out.println("---Kõik tehingud---");
        System.out.println(transactionsSet);

        Set<Transaction> transactions2Set = resource.getTransactionsByDate("2017-11-04","2017-11-04");
        System.out.println("piiratud tehingud");
        System.out.println(transactions2Set);


        //add transaction
        Transaction newAction = new Transaction()
                .setAmount(4)
                .setDateOfTransaction(new SimpleDateFormat("yyyy-MM-dd").parse("2017-11-13"))
                .setCategoryId(2)
                .setUserId(6)
                .setDescription("banaan");
        resource.addTransaction(newAction);
        transactionsSet = resource.getAllTransactions();
        System.out.println("---Kõik tehingud koos uue tehinguga---");
        System.out.println(transactionsSet);





        // delete transaction
        //resource.deleteTransactionById(11);
        transactionsSet = resource.getAllTransactions();
        System.out.println("---Kustutan ühe tehingu---");
        System.out.println(transactionsSet);

        newAction.setAmount(100).setDescription("avokaado").setTransactionId(42);
        resource.updateTransaction(newAction);
        System.out.println("---Muudan ühe tehingu---");
        System.out.println(transactionsSet);

    }
}
*/
