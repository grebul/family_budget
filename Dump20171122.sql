-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: family_budget
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(4) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(25) NOT NULL,
  `category_expense` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_id_UNIQUE` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Palk',0),(2,'Muud sissetulekud',0),(3,'Tervishoid',1),(4,'Laenud ja liisingud',1),(5,'Majapidamistarbed',1),(6,'Vaba aeg',1),(7,'Lapsed ja perekond',1),(8,'Kommunaalkulud',1),(9,'Toit',1),(10,'Haridus',1),(11,'Transport',1),(12,'Riided ja välimus',1),(40,'Investeeringud',1),(41,'Muud kulud',1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `group_id` int(4) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(15) NOT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_id_UNIQUE` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (1,'puud'),(2,'Teeleke'),(8,'Some name'),(17,'Some name'),(44,'Some name'),(45,'Some name'),(46,'Some name'),(47,'Some name'),(48,'Some name'),(49,'Some name'),(50,'Some name'),(51,'Some name'),(52,'Some name'),(53,'Some name'),(54,'Some name');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_category`
--

DROP TABLE IF EXISTS `group_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_category` (
  `group_cat_id` int(8) NOT NULL AUTO_INCREMENT,
  `group_id` int(4) NOT NULL,
  `category_id` int(4) NOT NULL,
  PRIMARY KEY (`group_cat_id`),
  UNIQUE KEY `group_cat_id_UNIQUE` (`group_cat_id`),
  KEY `group_id_idx` (`group_id`),
  KEY `cat_group_id_idx` (`category_id`),
  CONSTRAINT `cat_group_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `group_cat_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_category`
--

LOCK TABLES `group_category` WRITE;
/*!40000 ALTER TABLE `group_category` DISABLE KEYS */;
INSERT INTO `group_category` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,2,1),(11,2,2),(12,2,3),(13,2,4),(14,2,5),(15,2,6),(16,2,7),(17,2,8),(18,2,9),(19,2,10),(20,2,11),(21,2,12),(101,54,8),(102,54,9),(103,54,4),(104,54,11),(105,54,1),(106,54,2),(107,54,5),(108,54,41),(109,54,12),(110,54,7);
/*!40000 ALTER TABLE `group_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `potential_user`
--

DROP TABLE IF EXISTS `potential_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `potential_user` (
  `pot_user_id` int(4) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`pot_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `potential_user`
--

LOCK TABLES `potential_user` WRITE;
/*!40000 ALTER TABLE `potential_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `potential_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_transaction` date NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `amount` decimal(9,2) NOT NULL,
  `category_id` int(4) NOT NULL,
  `user_id` int(4) NOT NULL,
  `group_id` int(4) NOT NULL,
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `transaction_id_UNIQUE` (`transaction_id`),
  KEY `category_id_idx` (`category_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `trans_group_id_idx` (`group_id`),
  CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trans_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (2,'2017-11-01','palk',1750.00,1,1,1),(3,'2017-11-04','liising',236.50,3,1,1),(4,'2017-11-05','palk',1300.00,1,1,1),(20,'2017-11-01','intress',20.00,1,1,1),(21,'2017-11-01','intress',2.00,1,1,1),(25,'2017-11-03','lasteaia kulud',57.34,5,1,1),(26,'2017-10-01','kodulaen',300.00,3,1,1),(27,'2017-11-01','õhtusöök',34.00,4,1,1),(28,'2017-11-02','',64.00,1,1,1),(29,'2017-11-02','',32.00,1,1,1),(34,'2017-09-08','lasteaia kulud',57.35,5,1,1),(35,'2017-11-16','toit',1.05,4,1,1),(58,'2017-11-11','',53.00,1,1,1),(60,'2017-11-18','pp',45.00,1,1,1),(61,'2017-11-16','test',45.99,9,2,1),(62,'2017-11-01','laen',1000.00,1,2,1),(63,'2017-12-31','aastavahetuse pidu',100.00,11,2,1),(64,'2017-11-18','',88.00,11,2,1),(65,'2017-08-08','koduvalve',42.00,3,2,1),(66,'2017-11-16','pärandus',1000.00,1,1,1),(67,'2017-11-19','',654.00,1,2,1),(68,'2017-11-25','',52.00,1,2,1),(69,'2017-11-21','',52.00,1,2,1),(70,'2017-11-16','bussipilet',10.00,6,2,1),(71,'2017-11-16','kleit',10.00,7,2,1),(72,'2017-11-18','juuksur',31.00,7,3,2),(73,'2017-11-11','küünelakk',3.00,7,3,2),(74,'2017-11-11','',423.00,1,3,2),(75,'2017-11-12','peretoetus',321.00,1,3,2),(76,'2017-09-06','',99.00,1,3,2),(77,'2017-09-06','',99.00,1,3,2),(78,'2017-10-03','',900.00,1,3,2),(80,'2017-11-01','',54.00,1,1,1),(81,'2017-11-04','',31.00,1,1,1),(82,'2017-09-05','',53.00,1,1,1),(83,'2017-11-01','',312.00,1,1,1),(84,'2017-11-18','',1.00,12,1,1),(85,'2017-11-05','',78.00,10,1,1),(89,'2017-11-17','',15.00,1,2,1),(90,'2017-11-11','',45.00,1,2,1),(92,'2017-11-01','',87.00,8,2,1),(94,'2017-09-22','kodulaen',87000.00,3,2,1),(96,'2017-08-08','kleit',700.00,7,2,2),(97,'2017-11-17','ilus kleit',200.00,7,2,1),(98,'2017-11-05','üür',600.00,2,2,1),(101,'2017-10-05','palk',1300.00,1,2,1),(102,'2017-11-08','lastetoetus',100.00,1,2,1),(103,'2017-11-20','intress',1.20,1,2,1),(104,'2017-10-03','toit',25.02,4,2,1),(105,'2017-10-06','toit',45.00,4,2,1),(106,'2017-10-10','toit',45.00,4,2,1),(107,'2017-10-13','toit',54.05,4,2,1),(108,'2017-10-17','toit',32.09,4,2,1),(109,'2017-10-20','toit',36.99,4,2,1),(110,'2017-10-24','toit',26.00,4,2,1),(111,'2017-11-24','toit',56.21,4,2,1),(112,'2017-11-30','toit',58.65,4,2,1),(113,'2017-11-08','lastetoetus',100.00,1,2,1);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(4) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email` varchar(45) NOT NULL,
  `group_id` int(4) NOT NULL,
  `unconfirmed_email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `group_id_idx` (`group_id`),
  CONSTRAINT `group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Margus','Kask','margus.kask@gmail.com',1,NULL),(2,'Pille','Tamm','pille123@gmail.com',1,''),(3,'Teele','Kivi','teeeele@gmail.com',2,NULL),(5,'Kaja','Kull','kaja@gmail.com',2,NULL),(6,'Toomas','Toomingas','toomas.toomingas@gmail.com',8,''),(7,'Ele','Toomingas','ele.toomingas@gmail.com',8,''),(60,'Liisa','Iling','liisa.iling@gmail.com',54,''),(61,'Kristi','Luberg','kristi.luberg@gmail.com',54,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-22 16:14:56
